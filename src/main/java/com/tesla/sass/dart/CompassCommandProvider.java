package com.tesla.sass.dart;

import com.sencha.cli.CommandProvider;
import com.sencha.cli.Commands;

public class CompassCommandProvider implements CommandProvider {
    public void extendCommands(Commands parent) {
        System.out.println("compass provider");
        String name = parent.getCommandName(true);
        if ("sencha".equals(name))
            parent.addCommand("compass", SassCommands.class);
    }

    public String getAntlibResource() {
        return "com/tesla/sass/dart/ant/antlib.xml";
    }
}