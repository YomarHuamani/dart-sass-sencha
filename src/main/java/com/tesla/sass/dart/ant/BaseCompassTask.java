package com.tesla.sass.dart.ant;

import com.sencha.ant.BaseAntTask;
import com.sencha.util.Configuration;
import com.tesla.sass.dart.SassCommands;

import org.apache.tools.ant.Project;

public abstract class BaseCompassTask extends BaseAntTask {

    protected String _cssDir;

    protected String _sassDir;

    protected boolean _trace;

    protected abstract SassCommands.BaseSassCommand getCommand(SassCommands paramCompassCommands);

    public void setCssdir(String dir) {
        this._cssDir = dir;
    }

    public void setSassDir(String dir) {
        this._sassDir = dir;
    }

    public void setTrace(boolean enable) {
        this._trace = enable;
    }

    protected void doExecute() {
        System.out.println("=============== Firstbuild ===============");

        System.out.println(this._trace);

        Project proj = getProject();
        SassCommands compass = new SassCommands();
        Configuration config = compass.getConfiguration();

        if (config == null) {
            config = new Configuration();
            compass.setConfiguration(config);
        }

        for (Object key : proj.getProperties().keySet()) {
            String name = key.toString();
            config.set(name, proj.getProperty(name));
        }

        SassCommands.BaseSassCommand command = getCommand(compass);

        if (this._trace)
            command.setArg("--trace");

        command.setCssDir(proj.replaceProperties(this._cssDir));
        command.setSassDir(proj.replaceProperties(this._sassDir));
        command.execute();
    }
}
