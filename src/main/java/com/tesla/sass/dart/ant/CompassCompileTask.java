package com.tesla.sass.dart.ant;


import com.tesla.sass.dart.SassCommands;

public class CompassCompileTask extends BaseCompassTask {

    protected SassCommands.BaseSassCommand getCommand(SassCommands parent) {
        System.out.println("compass compile task");
        return parent.createCompile();
    }

}
