package com.tesla.sass.dart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sencha.cli.annotations.Doc;
import com.sencha.command.BaseSenchaCommand;
import com.sencha.logging.SenchaLogManager;
import com.sencha.util.PathUtil;
import com.sencha.util.StringUtil;
import com.sencha.util.annotations.Alias;

import org.slf4j.Logger;

@Doc("Wraps execution of compass for sass compilation")
public class SassCommands extends BaseSassCommands {
    private static final Logger _logger = SenchaLogManager.getLogger();


    public abstract class BaseSassCommand extends BaseSenchaCommand {
        protected List<String> sassArgs = new ArrayList<String>();

        protected String _cssDir;
        protected String _sassDir;

        public void setArg(String arg) {
            this.sassArgs.add(arg);
        }

        public List<String> getArgs() {
            return this.sassArgs;
        }

        @Doc("Sets the css-dir compass argument.")
        public void setCssDir(String cssDir) {
            this._cssDir = cssDir;
        }

        @Doc("Sets the sass-dir compass argument.")
        public void setSassDir(String sassDir) {
            this._sassDir = sassDir;
        }

        public void execute(@Alias("args") String... cliArgs) {

            System.out.println("=============== Second build ===============");
            Collections.addAll(getArgs(), cliArgs);

            List<String> args = new ArrayList<>();

            if (!StringUtil.isNullOrEmpty(this._sassDir)){
                Collections.addAll(args, PathUtil.getCanonicalPath(this._sassDir));
            }

            Collections.addAll(args, " ");

            if (!StringUtil.isNullOrEmpty(this._cssDir)){
                Collections.addAll(args, PathUtil.getCanonicalPath(this._cssDir));
            }

            args.addAll(this.sassArgs);

            System.out.println(sassArgs);

            System.out.println("=======================================");

            System.out.println(_cssDir);
            System.out.println(_sassDir);

            System.out.println("=======================================");
            System.out.println(args);


            runSassCommand(args.toArray(new String[0]));
        }

        public abstract String getSubCommand();
    }

    public class CompileCommand extends BaseSassCommand {
        public String getSubCommand() {
            return "compile";
        }
    }

    public CompileCommand createCompile() {
        return new CompileCommand();
    }
}
