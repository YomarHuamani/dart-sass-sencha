package com.tesla.sass.dart;

import com.sencha.cli.annotations.ConfigKey;
import com.sencha.cli.annotations.Doc;
import com.sencha.command.BaseSenchaCommands;
import com.sencha.exceptions.ExNotFound;
import com.sencha.exceptions.ExProcess;
import com.sencha.logging.SenchaLogManager;
import com.sencha.util.*;
import org.slf4j.Logger;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseSassCommands extends BaseSenchaCommands {

    private static final Logger _logger = SenchaLogManager.getLogger();
    protected String _nativeSassCommand = "sass";
    protected boolean _useNative = detectGlobalSass();

    public String getSassCommand() {
        return this._nativeSassCommand;
    }

    @Doc("Enables / disables calls to system installed sass dart native")
    public void setNative(boolean enable) {
        this._useNative = enable;
    }

    @ConfigKey("native.sass.path")
    @Doc("set the path to sass executable")
    public void setSassCommand(String path) {
        this._nativeSassCommand = path;
    }

    public void runSassCommand(String[] command) {

        List<String> sassArgs = new ArrayList<>();
        List<String> fullArgs = new ArrayList<>();

        if (this._useNative) {
            _logger.info("executing sass using system installed dart runtime");

            Collections.addAll(sassArgs, getSassCommand());
            fullArgs.addAll(sassArgs);
        }

        Collections.addAll(fullArgs, command);

        if (_logger.isDebugEnabled())
            _logger.debug("executing compass : {}", StringUtil.join(fullArgs, " "));

        System.out.println("*******************************************");
        System.out.println(fullArgs);

        ExternalProcess proc = ProcessUtil.exec(fullArgs.toArray(new String[0]));

        int eCode = proc.getExitCode();
        if (eCode > 0)
            throw (new ExProcess("sass process exited with non-zero code : {0}", eCode)).raise();
    }

    public boolean detectGlobalSass() {
        try {
            String[] command = { "sass", "--version" };
            ExternalProcess proc = ProcessUtil.exec(command);
            String output = proc.getOutput();
            if (output.length() > 0 && proc.getExitCode() == 0) {
                _logger.debug("sass detected, checking for version in : {}", proc.getOutput());
                Pattern verRegex = RegexUtil.getInstance().get("(\\d{1,5}\\.\\d{1,5}\\.\\d{1,5})", 42);
                Matcher match = verRegex.matcher(output);
                if (match.find())
                    return true;
                _logger.debug("unable to detect version number in output");
            }
        } catch (Exception ex) {
            return false;
        }
        return false;
    }
}
